<?php
//define('WP_HOME','http://localhost/mySkoolStore/');
//define('WP_SITEURL','http://localhost/mySkoolStore/');
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'myskoolstore' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'p455w0rd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YKEeM+ezAt+2XXFlww?vY9!sk,5>;.@Swc)Flp#dfjIdr#XnPTX4_jwN}?Jm<?AC' );
define( 'SECURE_AUTH_KEY',  ':g|h$aa&2A>m!6;3.-LaD}H~Ne)(rb!qVk%[/R>~M[Hf>7jSbRDnrxSNWv`^ke]+' );
define( 'LOGGED_IN_KEY',    'WP}x%,4l#^s:cth5<n$YA`<U^(p@C@`OXA|nVP[ >N)0v-k]z$8M)o!iG3>oN/t(' );
define( 'NONCE_KEY',        'p$c/+:obWR=DA9mtx6,R[W^7g%+=z;EiwEeX 5=7vqiN;<j}6Xz^})FH`i]ot.z_' );
define( 'AUTH_SALT',        'FL)uCN3vfUM| Yq]6-.Ns`:m$9Z%c5*wFfNiB@!u/X|ih7I(cHQG,E1L=Fd%aNYb' );
define( 'SECURE_AUTH_SALT', 'M!3WZkaqf107e# g5w]Fb6{|=b|:z?KDHQt{N3K*;YV=xeukFxJ,24mqk]Ne[V*L' );
define( 'LOGGED_IN_SALT',   ',OtYq9#uOq[CUdP&Q*2c># Rn=eBHB0?b&)br:YT+1%rA^nDKH;g u&?RwCEf}ox' );
define( 'NONCE_SALT',       '}45#oOeslMX}[6fS{;3gec+1/ueM1J]o5?=MVc;uHj.y+Y7/bp7*:~+X IN_Ya6q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

