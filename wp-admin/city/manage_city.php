<?php
/**
 * Edit City Administration Screen.
 *
 * @package WordPress
 * @subpackage Administration
 */
/** WordPress Administration Bootstrap */
require_once( dirname(__FILE__) . '/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );

global $wpdb;


$wp_list_table = $wpdb->prefix . 'city';
$pagenum = $wp_list_table->get_pagenum();
$title = __('Manage City');
$parent_file = 'manage_city.php';

$update = '';
$wp_list_table->prepare_items();
$total_pages = $wp_list_table->get_pagination_arg('total_pages');
if ($pagenum > $total_pages && $total_pages > 0) {
    wp_redirect(add_query_arg('paged', $total_pages));
    exit;
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php
		echo esc_html( $title );
		?>
</h1>
<?php 
if ( strlen( $usersearch ) ) {
	/* translators: %s: search keywords */
	printf( '<span class="subtitle">' . __( 'Search results for &#8220;%s&#8221;' ) . '</span>', esc_html( $usersearch ) );
}
?>
    <hr class="wp-header-end">
    <?php $wp_list_table->views(); ?>
    <form method="get">

		<?php $wp_list_table->search_box( __( 'Search Users' ), 'user' ); ?>

		
		<?php $wp_list_table->display(); ?>
</form>

<br class="clear" />
</div>
<?php
include( ABSPATH . 'wp-admin/admin-footer.php' );
?>