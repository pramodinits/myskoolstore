<?php
/**
 * Edit School Administration Screen.
 *
 * @package WordPress
 * @subpackage Administration
 */
/** WordPress Administration Bootstrap */
require_once( dirname(__FILE__) . '/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );

global $wpdb;
$table_name = $wpdb->prefix . 'school';
$table_name_city = $wpdb->prefix . 'city';

//get city list
$city_list = $wpdb->get_results("SELECT city_id, city_name FROM $table_name_city", OBJECT_K);
$result_city = array();
foreach ($city_list as $k => $v) {
    $result_city[$k] = $v->city_name;
}

if (isset($_POST['addschool'])) {
    $name = $_POST['school_name'];
    $desc = $_POST['description'];
    $city = $_POST['id_city'];
    $wpdb->query("INSERT INTO $table_name(school_name,id_city,description) VALUES('$name','$city','$desc')");
    echo "<script>window.location.reload();</script>";
}

if (isset($_POST['uptsubmit'])) {
    $id = $_POST['uptid'];
    $name = $_POST['uptname'];
    $desc = $_POST['uptdesc'];
    $city = $_POST['id_city'];
    $wpdb->query("UPDATE $table_name SET school_name='$name',id_city='$city',description='$desc' WHERE school_id='$id'");
    echo "<script>location.replace('manage_school.php')</script>";
}

if (isset($_GET['del'])) {
    $del_id = $_GET['del'];
    $wpdb->query("DELETE FROM $table_name WHERE school_id='$del_id'");
    echo "<script>location.replace('manage_school.php')</script>";
}
?>
<style>
    .wp-list-table tr td:first-child{
        text-align: center;
        color:#0073aa;
        font-weight: bold;
        font-size: 15px;
    }
    .wp-list-table tr td {
        font-size: 15px;
    }
    button {
        cursor: pointer;
    }
    .regular-text {
        width: 21em;
    }
</style>
<div class="wrap">
    <h1 class="wp-heading-inline">
        <?php
        echo esc_html($title);
        ?>
    </h1>

    <hr class="wp-header-end">
    <table class="wp-list-table widefat striped" id="wp-list-table">
        <thead>
            <tr>
                <th width="25%">School Name</th>
                <th width="25%">City Name</th>
                <th width="25%">Description</th>
                <th width="25%">Actions</th>
            </tr>
        </thead>
        <tbody>
        <form action="" method="post">
            <tr>
                <td><input class="regular-text" type="text" id="school_name" name="school_name" required=""></td>
                <td>
                    <select class="regular-text" id="id_city" name="id_city" required="">
                        <option value="">Select City</option>
                        <?php foreach ($result_city as $k => $v) { ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <?php }
                        ?>
                    </select>    
                </td>
                <td>
                    <textarea class="regular-text" name="description" id="description"></textarea>
                </td>
                <td><button id="addschool" name="addschool" type="submit">Add School</button></td>
            </tr>
        </form>
        <?php
        $result = $wpdb->get_results("SELECT * FROM $table_name");
        foreach ($result as $print) {
            $desc = $print->description ? $print->description : "--";
            echo "
              <tr>
                <td width='25%'>$print->school_name</td>
                <td width='25%'>" . $result_city[$print->id_city] . "</td>
                <td width='25%'>$desc</td>
                <td width='25%'><a href='manage_school.php?upt=$print->school_id'>"
            . "<button type='button'><span title='Edit' class='dashicons dashicons-edit'></span></button></a> "
            . "<a href='manage_school.php?del=$print->school_id' class='confirmation'>
                        <button type='button'><span title='Delete' class='dashicons dashicons-trash'></span></button></a></td>
              </tr>
            ";
        }
        ?>
        </tbody>  
    </table>

    <br class="clear" />
    <br/>
    <?php
    if (isset($_GET['upt'])) {
        $upt_id = $_GET['upt'];
        $result = $wpdb->get_results("SELECT * FROM $table_name WHERE school_id='$upt_id'");
        $cities = "";
        foreach ($result as $print) {
            $name = $print->school_name;
            $desc = $print->description;
            foreach ($result_city as $k => $v) {
                $selected = $k == $print->id_city ? "selected" : "";
                $cities .= "<option value='$k' $selected>$v</option>";
            }
        }

        echo "
        <table class='wp-list-table widefat striped'>
          <thead>
            <tr>
              <th width='25%'>School Name</th>
              <th width='25%'>City Name</th>
              <th width='25%'>School description</th>
              <th width='25%'>Actions</th>
            </tr>
          </thead>
          <tbody>
            <form action='' method='post'>
            <input type='hidden' id='uptid' name='uptid' value='$print->school_id'>
              <tr>
                <td width='25%'><input type='text' required class='regular-text' id='uptname' name='uptname' value='$print->school_name'></td>
                <td width='25%'>
                <select class='regular-text' required name='id_city'>
                $cities
                </select>
                </td>
                <td width='25%'><textarea name='uptdesc' class='regular-text' id='uptdesc'>$print->description</textarea>
                </td>
                <td width='25%'><button id='uptsubmit' name='uptsubmit' type='submit'>UPDATE</button> 
                <a href='manage_school.php'><button type='button'>CANCEL</button></a></td>
              </tr>
            </form>
          </tbody>
        </table>";
    }
    ?>

</div>
<script type="text/javascript">
    jQuery(function ($) {
        $('.confirmation').on('click', function () {
            return confirm('Are you sure to remove?');
        });
    });

</script>
<?php
include( ABSPATH . 'wp-admin/admin-footer.php' );
?>