<?php
/**
 * Edit City Administration Screen.
 *
 * @package WordPress
 * @subpackage Administration
 */
/** WordPress Administration Bootstrap */
require_once( dirname(__FILE__) . '/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );

global $wpdb;
$table_name = $wpdb->prefix . 'city';

if (isset($_POST['addcity'])) {
    $name = $_POST['city_name'];
    $desc = $_POST['description'];
    $wpdb->query("INSERT INTO $table_name(city_name,description) VALUES('$name','$desc')");
    echo "<script>window.location.reload();</script>";
}

if (isset($_POST['uptsubmit'])) {
    $id = $_POST['uptid'];
    $name = $_POST['uptname'];
    $desc = $_POST['uptdesc'];
    $wpdb->query("UPDATE $table_name SET city_name='$name',description='$desc' WHERE city_id='$id'");
    echo "<script>location.replace('manage_city.php')</script>";
}

if (isset($_GET['del'])) {
    $del_id = $_GET['del'];
    $wpdb->query("DELETE FROM $table_name WHERE city_id='$del_id'");
    echo "<script>location.replace('manage_city.php')</script>";
}
?>
<style>
    .wp-list-table tr td:first-child{
        text-align: center;
        color:#0073aa;
        font-weight: bold;
        font-size: 15px;
    }
    .wp-list-table tr td {
        font-size: 15px;
    }
    button {
        cursor: pointer;
    }
</style>
<div class="wrap">
    <h1 class="wp-heading-inline">
        <?php
        echo esc_html($title);
        ?>
    </h1>

    <hr class="wp-header-end">
    <table class="wp-list-table widefat striped" id="wp-list-table">
        <thead>
            <tr>
                <th width="25%">City name</th>
                <th width="25%">Description</th>
                <th width="25%">Actions</th>
            </tr>
        </thead>
        <tbody>
        <form action="" method="post">
            <tr>
                <td><input class="regular-text" type="text" id="city_name" name="city_name" required=""></td>
                <td>
                    <textarea class="regular-text" name="description" id="description"></textarea>
                </td>
                <td><button id="addcity" name="addcity" type="submit">Add City</button></td>
            </tr>
        </form>
        <?php
        $result = $wpdb->get_results("SELECT * FROM $table_name");
        foreach ($result as $print) {
            $desc = $print->description ? $print->description : "--";
            echo "
              <tr>
                <td width='35%'>$print->city_name</td>
                <td width='25%'>$desc</td>
                <td width='25%'><a href='manage_city.php?upt=$print->city_id'>"
            . "<button type='button'><span title='Edit' class='dashicons dashicons-edit'></span></button></a> "
            . "<a href='manage_city.php?del=$print->city_id' class='confirmation'>
                        <button type='button'><span title='Delete' class='dashicons dashicons-trash'></span></button></a></td>
              </tr>
            ";
        }
        ?>
        </tbody>  
    </table>

    <br class="clear" />
    <br/>
    <?php
    if (isset($_GET['upt'])) {
        $upt_id = $_GET['upt'];
        $result = $wpdb->get_results("SELECT * FROM $table_name WHERE city_id='$upt_id'");
        foreach ($result as $print) {
            $name = $print->city_name;
            $desc = $print->description;
        }
        echo "
        <table class='wp-list-table widefat striped'>
          <thead>
            <tr>
              <th width='35%'>City Name</th>
              <th width='25%'>City description</th>
              <th width='25%'>Actions</th>
            </tr>
          </thead>
          <tbody>
            <form action='' method='post'>
            <input type='hidden' id='uptid' name='uptid' value='$print->city_id'>
              <tr>
                <td width='35%'><input type='text' required class='regular-text' id='uptname' name='uptname' value='$print->city_name'></td>
                <td width='25%'><textarea name='uptdesc' class='regular-text' id='uptdesc'>$print->description</textarea>
                </td>
                <td width='25%'><button id='uptsubmit' name='uptsubmit' type='submit'>UPDATE</button> 
                <a href='manage_city.php'><button type='button'>CANCEL</button></a></td>
              </tr>
            </form>
          </tbody>
        </table>";
    }
    ?>

</div>
<script type="text/javascript">
    jQuery(function ($) {
        $('.confirmation').on('click', function () {
            return confirm('Are you sure to remove?');
        });
    });

</script>
<?php
include( ABSPATH . 'wp-admin/admin-footer.php' );
?>