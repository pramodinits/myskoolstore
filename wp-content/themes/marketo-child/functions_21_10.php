<?php
/* ---------------------------------------------------
 * Theme: Charitious - WordPress Theme
 * Author: XpeedStudio
 * Author URI: http://www.xpeedstudio.com
  -------------------------------------------------- */

function marketo_theme_enqueue_styles() {

    $parent_style = 'parent-style';

    if (!wp_style_is($parent_style, $list = 'enqueued')) {
        wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css', array());
    }
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));
    wp_enqueue_style('child-autocomplete-js', get_stylesheet_directory_uri() . '/autocomplete/css/jquery.magicsearch.css', array($parent_style));

    wp_enqueue_script('child-custom', get_stylesheet_directory_uri() . '/custom.js', array('jquery'), '', true);
    wp_enqueue_script('child-autocomplete-js', get_stylesheet_directory_uri() . '/autocomplete/js/jquery.magicsearch.js', array('jquery'), '', true);
}

add_action('wp_enqueue_scripts', 'marketo_theme_enqueue_styles', 99);

function my_login_logo_one() {
    ?> 
    <style type="text/css"> 
        body.login div#login h1 a {
            background-image: url(<?php echo site_url() ?>/wp-content/uploads/2020/05/Logo_1.png); 
            width: 100%;
            background-size: 150px;
            padding-bottom: 10px;
        } 
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'my_login_logo_one');

/* add_filter('pre_get_posts', 'custom_pre_get_posts');

  function custom_pre_get_posts($query) {
  if (is_search()) {
  $query->set('post_type', 'product');
  }

  return $query;
  }
 */
//add custom menu
add_action('admin_menu', 'custom_menu');

function custom_menu() {
//  add_menu_page( 
//      'City', 
//      'Manage City', 
//      'edit_posts', 
//      'manage_city', 
//      'manage_city.php', 
//      'dashicons-location',
//        3
//     );
    add_menu_page(
            __('Manage City', 'textdomain'), 'Manage City', 'manage_options', 'manage_city.php', '', 'dashicons-location', 3
    );
    add_menu_page(
            __('Manage School', 'textdomain'), 'Manage School', 'manage_options', 'manage_school.php', '', 'dashicons-store', 3
    );
}

//add new table
?>

<?php
//$current_url="//".$_SERVER['HTTP_REFERER'].$_SERVER['REQUEST_URI'];
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$my_account = get_permalink(get_page_by_path('my-account'));
$home = get_permalink(get_page_by_path('store-listing'));
if (!is_user_logged_in() && ($actual_link == $my_account || $actual_link == $home)) {
    ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            
            $('#vendor-city-reg').change(function () {
                var selected_val = $("#vendor-city-reg").val();
                $.ajax(
                        {
                            type: 'post',
                            data: {city_id: selected_val},
                            success: function (res) {
                                $("#vendor-school-reg").html(res);
                            }

                        });
            });


            $("#vendor-city-edit").change(function () {
                var selected_val = $("#vendor-city-edit").val();
                $.ajax(
                        {
                                type: 'post',
                                data: {city_id_edit: selected_val},
                                success: function (res) {
                                    $.each(res.d, function(k, v) {
                                        console.log(k + v);
                                    })
    //                                $("#vendor-school-edit").html(res);
                                    $(".vendor-school-edit").html(res);
                                }

                            });
            });
        });


        
    </script>
<?php }
?>


<?php
if (isset($_POST['city_id_edit'])) {
    global $wpdb;
    $city_id = $_POST['city_id_edit'];
//    $userid = $_POST['userid'];
//    $user = get_user_by('id', $userid);
    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id", OBJECT_K);

//    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id AND"
//            . " school_id NOT IN (Select meta_value from wp_usermeta where meta_key='vendor_school')", OBJECT_K);
//    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id AND"
//            . " school_id NOT IN (Select meta_value from wp_usermeta where meta_key='vendor_school')", OBJECT_K);
    $result_school = array();
    $str = "<option value=''>Select School</option>";
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }
    foreach ($result_school as $key => $val) {
        $str .= "<option value='{$key}'>" . $val . "</option>";
    }
    echo json_encode($result_school);
//    echo $str;
    exit;
}

if (isset($_POST['city_id'])) {
    global $wpdb;
    $city_id = $_POST['city_id'];
    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id", OBJECT_K);
//    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id AND"
//            . " school_id NOT IN (Select meta_value from wp_usermeta where meta_key='vendor_school')", OBJECT_K);
    $result_school = array();
    $str = "<option value=''>Select School</option>";
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }
    foreach ($result_school as $key => $val) {
        $str .= "<option value='{$key}'>" . $val . "</option>";
    }
    echo $str;
    exit;
}

/**
 * Get city lists
 */
//edit new custom fields vendor account
function endereco($current_user, $profile_info) {
    global $wpdb;
    $user = get_user_by('id', get_current_user_id());
    $table_name = $wpdb->prefix . 'school';
    $table_name_city = $wpdb->prefix . 'city';

    $city_list = $wpdb->get_results("SELECT city_id, city_name FROM $table_name_city", OBJECT_K);
    $result_city = $result_school = $city_listing = array();
    foreach ($city_list as $k => $v) {
        $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $k AND"
                    . " school_id NOT IN (Select meta_value from wp_usermeta where meta_key='vendor_school')", OBJECT_K);

            $city_listing['id'] = $k;
            $city_listing['name'] = $v->city_name;
            foreach($school_list as $key => $val) {
                $city_listing['school'] = array('school_id' => $key, 'school_name' => $val->school_name);
            }
            $result_city[$k] = $v->city_name;
        }
//                    print "<pre>";print_r($city_listing);

//    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM $table_name where id_city = $user->vendor_city AND school_id = $user->vendor_school", OBJECT_K);
//    foreach ($school_list as $k => $v) {
//        $result_school[$k] = $v->school_name;
//    }
    ?> 
    <script type="text/javascript">
        $(document).ready(function () {
            //add more school
            
                var wrapper = $(".input_fields_wrap"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID
                var x = 1; //initlal text box count
                var appendElement = '<div class="dokan-form-group">\n\
               <div class="dokan-form-group dokan-w5 dokan-left"  id="cityParent_'+x+'" style="margin-right: 10px;"> \n\
<select class="dokan-form-control"  onchange="getSchool(this);" name="vendor_city[]" required="">\n\
<option value="">Select City</option>\n\
<?php foreach ($result_city as $k => $v) { ?>\n\
\n\
<option value="<?= $k ?>"><?= $v ?></option>\n\
<?php } ?>\n\
</select></div>'+
'<div class="dokan-form-group dokan-w6 dokan-left" style="margin-right: 10px;">\n\
<input class="magicsearch dokan-form-control" id="basic_'+x+'" placeholder="Search Schools...">\n\
</div>'+
                '<a href="#" class="remove_field"><i class="fa fa-times font16 text-danger"  style="border: 1px solid;border-radius: 7px;padding: 5px;" aria-hidden="true">\n\
    <span>Remove School</span></i></a>\n\
<div class="dokan-clearfix"></div>\n\
</div>';
                
                $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        x++; //text box increment
        //make array
        var selectedArray=[]; 
     $('select[name="vendor_city[]"] option:selected').each(function() {
      selectedArray.push($(this).val());
     });
        
        //remove selected array
        $("div[id='cityParent_"+x+"'] > select > option").each(function() {
//      var myGroups = ["Cars", "Bikes", "Airplanes", "Motorcycles"];
      if($.inArray($(this).value(), selectedArray )===-1){
      $(this).remove();
      }
    });
        console.log(selectedArray);
                $(wrapper).append(appendElement); //add input box
        });
                $(wrapper).on("click", ".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
        })
            
        });
        </script>
    <input type="hidden" name="userid" id="userid" value="<?= get_current_user_id(); ?>">
    <input type="hidden" name="school_ids" id="school_ids">
    <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label"><?php _e('Distributor City & School', 'dokan'); ?></label>

            <div class="dokan-w5 dokan-text-left dokan-address-fields">
                <div class="input_fields_wrap">
                    <?php
                    if($user->vendor_city && $user->vendor_school) { ?>
                        
                    <?php } else { 
                        
                        ?>
                    <div class="dokan-form-group">
                                <div class="dokan-form-group dokan-w5 dokan-left" id="cityParent_0" style="margin-right: 10px;">
                                    <select class="dokan-form-control" onchange="getSchool(this);" name="vendor_city[]" required="">
                                        <option value="">Select City</option>
                                        <?php foreach ($result_city as $k => $v) { ?>
                                            <option value="<?= $k ?>"><?= $v ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div class="dokan-form-group dokan-w6 dokan-left" style="margin-right: 10px; max-width: 50%;">
                                    <input class="magicsearch dokan-form-control" id="basic_0" placeholder="Search Schools...">
                                </div>
                                
                                <div class="dokan-clearfix"></div>
                                <a class="add_field_button" href="#" style="border: 1px solid;border-radius: 7px;padding: 5px;">
                                    <i class="fa fa-plus font16 text-success" aria-hidden="true"></i> <span>Add more Schools</span>
                                </a>
                            </div>
                    <?php }
                    ?>
                    
                </div>
            </div>
        </div>
                    
    <?php
}

add_filter('dokan_settings_after_banner', 'endereco', 10, 2);

/**
 * Save the extra fields. Vendor Edit information
 *
 * @param  int  $customer_id Current customer ID.
 *
 * @return void
 */
function save_extra_endereco_fields($store_id, $dokan_settings) {
    $post_data = wp_unslash($_POST);
    print "<pre>"; print_r($post_data['vendor_city']);
    print_r($post_data['vendor_school']);exit;

//    update_user_meta($store_id, 'dokan_profile_settings', $dokan_settings);
    update_user_meta($store_id, 'vendor_city', sanitize_text_field($post_data['vendor_city']));
    update_user_meta($store_id, 'vendor_school', sanitize_text_field($post_data['vendor_school']));
}

add_action('dokan_store_profile_saved', 'save_extra_endereco_fields', 10, 2);

/**
 * Set field as a required one
 *
 * @since 1.0.0
 *
 * @param array $required_fields
 *
 * @return array
 */
function dokan_custom_seller_registration_required_fields($required_fields) {
    $required_fields['vendor_city'] = __('Please enter selling city', 'dokan-custom');
    $required_fields['vendor_school'] = __('Please enter selling school', 'dokan-custom');

    return $required_fields;
}

;
add_filter('dokan_seller_registration_required_fields', 'dokan_custom_seller_registration_required_fields');

/**
 * Add field in the registration form
 *
 */
function dokan_custom_seller_registration_field_after() {
    $post_data = wp_unslash($_POST);

    /**
     * Get city lists
     */
    global $wpdb;
    $table_name = $wpdb->prefix . 'school';
    $table_name_city = $wpdb->prefix . 'city';

    $city_list = $wpdb->get_results("SELECT city_id, city_name FROM $table_name_city", OBJECT_K);
    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM $table_name", OBJECT_K);
    $result_city = $result_school = array();
    foreach ($city_list as $k => $v) {
        $result_city[$k] = $v->city_name;
    }
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }
    ?>
    <p class="form-row form-group form-row-wide">
        <label for="vendor-city"><?php esc_html_e('Distributor City', 'dokan-custom'); ?><span class="required">*</span></label>
        <select class="form-control" id="vendor-city-reg" name="vendor_city" required="">
            <option value="">Select City</option>
            <?php foreach ($result_city as $k => $v) { ?>
                <option value="<?= $k ?>"><?= $v ?></option>
            <?php }
            ?>
        </select>
    </p>
    <p class="form-row form-group form-row-wide">
        <label for="vendor-school"><?php esc_html_e('Distributor School', 'dokan-custom'); ?><span class="required">*</span></label>
        <select class="form-control" id="vendor-school-reg" name="vendor_school" required="">
            <option value="">Select School</option>

        </select>
    </p>
    <?php
}

/**
 * Override the global/seller-registration-form template and add new field
 *
 * @since 1.0.0
 *
 * @param string $template
 * @param string $slug
 * @param string $name
 *
 * @return string
 */
//function dokan_custom_get_template_part($template, $slug, $name) {
//    if ('global/seller-registration-form' === $slug) {
//        $template = dirname(__FILE__) . '/dokan-seller-registration-form.php';
//    }
//
//    return $template;
//}

add_filter('dokan_seller_registration_field_after', 'dokan_custom_seller_registration_field_after');

/**
 * Save custom field data registration
 *
 * @since 1.0.0
 *
 * @param int   $vendor_id
 * @param array $dokan_settings
 *
 * @return void
 */
function dokan_custom_new_seller_created($vendor_id, $dokan_settings) {
    $post_data = wp_unslash($_POST);

    $vendor_city = sanitize_text_field($post_data['vendor_city']);
    $vendor_school = sanitize_text_field($post_data['vendor_school']);

    /**
     * This will save agent_id value with the `dokan_custom_agent_id` user meta key
     */
    update_user_meta($vendor_id, 'vendor_city', $vendor_city);
    update_user_meta($vendor_id, 'vendor_school', $vendor_school);
}

add_action('dokan_new_seller_created', 'dokan_custom_new_seller_created', 10, 2);

function get_store_url($atts) {

    $a = shortcode_atts(
            array(
        'seller_ids' => '',
            ), $atts);
//$seller_id  = get_user_by( 'id', $a['id'] );
//$vendor = dokan()->vendor->get( $seller_id );
//
    return $a;
}

/*
 * Add custom tax options
 */

add_action('woocommerce_product_options_tax', 'wc_custom_add_custom_fields');

function wc_custom_add_custom_fields() {
    global $post;

    $input_taxrate_inter = get_post_meta($post->ID, '_tax_rate_interstate', true);
    $input_taxrate_intra = get_post_meta($post->ID, '_tax_rate_intrastate', true);


    $all_tax_rates = $taxclasses = [];
    $options_inter[''] = __('Select tax rate', 'woocommerce'); // default value
    $options_intra[''] = __('Select tax rate', 'woocommerce'); // default value

    $tax_classes = WC_Tax::get_tax_classes(); // Retrieve all tax classes.
    if (!in_array('', $tax_classes)) { // Make sure "Standard rate" (empty class name) is present.
        array_unshift($tax_classes, '');
    }
    foreach ($tax_classes as $tax_class) { // For each tax class, get all rates.
        //$taxes = WC_Tax::get_rates_for_tax_class($tax_class); // for all tax class rates
        $taxes = WC_Tax::get_rates_for_tax_class(''); // for only standard class
        $all_tax_rates = array_merge($all_tax_rates, $taxes);
    }
    foreach ($all_tax_rates as $key => $val) {
        if ($val->tax_rate_name == "GST") {
            $options_inter[$val->tax_rate_id] = $val->tax_rate_name . "-" . round($val->tax_rate) . "";
        } else if ($val->tax_rate_name == "IGST") {
            $options_intra[$val->tax_rate_id] = $val->tax_rate_name . "-" . round($val->tax_rate) . "";
        }
    }

    woocommerce_wp_select(
            array(
                'id' => '_tax_rate_interstate',
                'value' => $input_taxrate_inter,
                'label' => __('Inter-state Tax Rate', 'woocommerce'),
                'options' => $options_inter,
                'desc_tip' => 'true',
                'description' => __('Choose a tax class for this product. Tax classes are used to apply different tax rates specific to certain types of product.', 'woocommerce'),
            )
    );

    woocommerce_wp_select(
            array(
                'id' => '_tax_rate_intrastate',
                'value' => $input_taxrate_intra,
                'label' => __('Intra-state Tax Rate', 'woocommerce'),
                'options' => $options_intra,
                'desc_tip' => 'true',
                'description' => __('Choose a tax class for this product. Tax classes are used to apply different tax rates specific to certain types of product.', 'woocommerce'),
            )
    );
}

add_action('woocommerce_process_product_meta', 'wc_custom_save_custom_fields');

function wc_custom_save_custom_fields($post_id) {
    $intrastate_tax = $_POST['_tax_rate_intrastate'];
    $interstate_tax = $_POST['_tax_rate_interstate'];
    update_post_meta($post_id, '_tax_rate_intrastate', $intrastate_tax);
    update_post_meta($post_id, '_tax_rate_interstate', $interstate_tax);
}

if (isset($_POST['tax_class'])) {
    $tax_class = $_POST['tax_class'] ? $_POST['tax_class'] : '';
    $taxes = WC_Tax::get_rates_for_tax_class();
    $taxclasses = array();
    foreach ($taxes as $k => $v) {
        $taxclasses[$k] = $v->tax_rate_name . " - (" . round($v->tax_rate) . "%)";
    }
    foreach ($taxclasses as $key => $val) {
        $str .= "<option value='{$key}'>" . $val . "</option>";
    }
    echo $str;
    exit;
}


/*
 * Get item quantity from cart
 */

function get_item_qty($product) {
    foreach (WC()->cart->get_cart() as $cart_item)
    // for variable products (product varations)
        $product_id = $product->get_parent_id();
    if ($product_id == 0 || empty($product_id))
        $product_id = $product->get_id();

    if ($product_id == $cart_item['product_id']) {
        return $cart_item['quantity'];
        // break;
    }
    return;
}

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_filter('woocommerce_product_related_posts_query', '__return_empty_array', 100);

/*
 * Woocommerce order invoice in email
 */
add_filter('wpo_wcpdf_custom_attachment_condition', 'wpo_wcpdf_processing_accepted_email', 100, 4);

function wpo_wcpdf_processing_accepted_email($condition, $order, $email_id, $document_type) {
    // disable attachment of the invoice to the processing email if the status is not order-accepted
    if ($document_type == 'invoice' && $email_id == 'customer_processing_order' && $order->get_status() != 'order-accepted') {
        return false;
    } else {
        return $condition;
    }
}

/*
 * Send email to customer on place order
 */

function sv_conditional_email_recipient($recipient, $order) {
    // Bail on WC settings pages since the order object isn't yet set yet
    // Not sure why this is even a thing, but shikata ga nai
    $page = $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : '';
    if ('wc-settings' === $page) {
        return $recipient;
    }

    // just in case
    if (!$order instanceof WC_Order) {
        return $recipient;
    }
    $items = $order->get_items();

    // check if a shipped product is in the order   
    foreach ($items as $item) {
        $product = $order->get_product_from_item($item);

        // add our extra recipient if there's a shipped product - commas needed!
        // we can bail if we've found one, no need to add the recipient more than once
        if ($product && $product->needs_shipping()) {
            $recipient .= "," . $order->get_billing_email();
            return $recipient;
        }
    }

    return $recipient;
}

//add_filter('woocommerce_email_recipient_new_order', 'sv_conditional_email_recipient', 10, 2);

/*
 * Order Cancellation email to customer
 */
function wc_cancelled_order_add_customer_email($recipient, $order) {
    return $recipient . ',' . $order->billing_email;
}

add_filter('woocommerce_email_recipient_cancelled_order', 'wc_cancelled_order_add_customer_email', 10, 2);


/*
 * Order cancellation from customer end
 */
/*
  add_filter( 'woocommerce_valid_order_statuses_for_cancel', 'custom_valid_order_statuses_for_cancel', 10, 2 );
  function custom_valid_order_statuses_for_cancel( $statuses, $order ){

  // Set HERE the order statuses where you want the cancel button to appear
  $custom_statuses    = array( 'pending', 'processing', 'on-hold', 'failed' );

  // Set HERE the delay (in days)
  $duration = 3; // 3 days

  // UPDATE: Get the order ID and the WC_Order object
  if( isset($_GET['order_id']))
  $order = wc_get_order( absint( $_GET['order_id'] ) );

  $delay = $duration*24*60*60; // (duration in seconds)
  $date_created_time  = strtotime($order->get_date_created()); // Creation date time stamp
  $date_modified_time = strtotime($order->get_date_modified()); // Modified date time stamp
  $now = strtotime("now"); // Now  time stamp

  // Using Creation date time stamp
  if ( ( $date_created_time + $delay ) >= $now ) return $custom_statuses;
  else return $statuses;
  } */

add_filter('woocommerce_valid_order_statuses_for_cancel', 'custom_valid_order_statuses_for_cancel', 10, 1);

function custom_valid_order_statuses_for_cancel($statuses) {

    // Set HERE the order statuses where you want the cancel button to appear
    return array_merge($statuses, array('processing', 'shipped'));
}

/*
 * update order meta in checkout
 */
add_action('woocommerce_checkout_update_order_meta', 'custom_checkout_field_update_order_meta');

function custom_checkout_field_update_order_meta($order_id) {
    session_start();
    if ($_SESSION['cartDetails'])
        update_post_meta($order_id, '_order_total', esc_attr(htmlspecialchars($_SESSION['cartDetails']['totalAmount'])));
    update_post_meta($order_id, '_order_tax_label', esc_attr(htmlspecialchars($_SESSION['cartDetails']['taxLabel'])));
    update_post_meta($order_id, '_order_tax', esc_attr(htmlspecialchars($_SESSION['cartDetails']['taxAmount'])));
    unset($_SESSION['cartDetails']);
}

add_action('woocommerce_product_query', 'Add_custom_search');

function Add_custom_search($query) {
    if (!is_admin() && $query->is_main_query()) {
        $query->set('author', $_SESSION['distributors_search_ids']);
        
        
        if ($query->is_search()) {
            $meta_query = $query->get('meta_query');

//            $query->set('post_status', array('publish', 'inherit'));
            $meta_query[] = array(
                'key' => '_product_attributes',
                'value' => $query->query['s'],
                'compare' => 'LIKE'
            );
//            $meta_query['tax_query'][] = array(
//            'taxonomy' => 'pa_class',
//            'terms'    => $query->query['s'],
//            'operator' => 'LIKE',
//        );
            $query->set('meta_query', $meta_query);

            add_filter('get_meta_sql', function( $sql ) {
                global $wpdb;

                static $nr = 0;
                if (0 != $nr++)
                    return $sql;

                $sql['where'] = mb_eregi_replace('^ AND', ' OR', $sql['where']);

//                $sql['where'] = sprintf(
//                " AND ( %s OR %s ) ",
//                $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
//                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
//            );
                return $sql;
            });
        }
    }
    return $query;
}

/*
 * Add custom menu in vendor dashboard
 */
add_filter('dokan_query_var_filter', 'dokan_load_document_menu');

function dokan_load_document_menu($query_vars) {
    $query_vars['deliveryboy'] = 'deliveryboy';
    $query_vars['add-deliveryboy'] = 'add-deliveryboy';
    $query_vars['action'] = 'action';
    return $query_vars;
}

add_filter('dokan_get_dashboard_nav', 'dokan_add_help_menu');

function dokan_add_help_menu($urls) {
    $urls['deliveryboy'] = array(
        'title' => __('Delivery Boy', 'dokan'),
        'icon' => '<i class="fa fa-shopping-bag"></i>',
        'url' => dokan_get_navigation_url('deliveryboy'),
        'pos' => 51
    );
    return $urls;
}

add_action('dokan_load_custom_template', 'dokan_load_template');

function dokan_load_template($query_vars) {
    if (isset($query_vars['deliveryboy'])) {
        get_template_part('deliveryboy/deliveryboy');
    }
    if (isset($query_vars['action'])) {
        get_template_part('deliveryboy/action');
    }
    if (isset($query_vars['add-deliveryboy'])) {
        get_template_part('deliveryboy/add_deliveryboy');
    }
}

add_action('init', 'do_output_buffer');

function do_output_buffer() {
    ob_start();
}

function my_register_route() {
//$custom = get_stylesheet_directory_uri() . '/api-custom.php';
    register_rest_route('v1', 'verifyMobileNo', array(
        'methods' => 'POST',
        'callback' => 'verifyMobileNo'
            )
    );
    register_rest_route('v1', 'setRestPassword', array(
        'methods' => 'POST',
        'callback' => 'setRestPassword'
            )
    );
    register_rest_route('v1', 'generateOTP', array(
        'methods' => 'POST',
        'callback' => 'generateOTP'
            )
    );
    register_rest_route('v1', 'verifyOTP', array(
        'methods' => 'POST',
        'callback' => 'verifyOTP'
            )
    );
    register_rest_route('v1', 'login', array(
        'methods' => 'POST',
        'callback' => 'login'
            )
    );
    register_rest_route('v1', 'getOrderList', array(
        'methods' => 'POST',
        'callback' => 'getOrderList'
            )
    );
    register_rest_route('v1', 'verifyDeliveryCode', array(
        'methods' => 'POST',
        'callback' => 'verifyDeliveryCode'
            )
    );
    register_rest_route('v1', 'updateOrderStatus', array(
        'methods' => 'POST',
        'callback' => 'updateOrderStatus'
            )
    );
    register_rest_route('v1', 'regenerateVerificationCode', array(
        'methods' => 'POST',
        'callback' => 'regenerateVerificationCode'
            )
    );
}

function verifyMobileNo(WP_REST_Request $request) {
    global $wpdb;
    $mobile_no = $request['mobile_no'];
//    $query = "SELECT * FROM {$wpdb->prefix}delivery_boy WHERE registered_phone = $mobile_no";
    $results = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}delivery_boy WHERE registered_phone = $mobile_no");
    if ($results->registered_phone) {
        $pwd_results = $wpdb->get_row("SELECT password FROM {$wpdb->prefix}delivery_boy WHERE registered_phone = $mobile_no");
        if ($pwd_results->password) {
            $data['Message'] = "This number is registered.";
            $data['StatusCode'] = 200;
            return rest_ensure_response($data);
        } else {
            $data['Message'] = "Password need to be set.";
            $data['StatusCode'] = 200;
            return rest_ensure_response($data);
        }
    } else {
        $data['Message'] = "This number($mobile_no) is not registered.";
        $data['StatusCode'] = 400;
        return rest_ensure_response($data);
    }
}

function setRestPassword(WP_REST_Request $request) {
    global $wpdb;
//    $id = $request['delivery_boy_id'];
    $mobile_no = $request['mobile_no'];
    $password = $request['password'];
    $imei_no = $request['IMEI_no'];
    if (isset($mobile_no) && isset($password) && $mobile_no && $password) {
        $upd['password'] = md5($password);
        $upd['IMEI'] = $imei_no;
        $wpdb->update('wp_delivery_boy', $upd, array('registered_phone' => $mobile_no));
        $data['Message'] = "Password has been set successfully.";
        $data['StatusCode'] = 200;
        return rest_ensure_response($data);
    }
}

function generateOTP(WP_REST_Request $request) {
    global $wpdb;
    $mobile_no = $request['mobile_no'];
    $results = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}delivery_boy WHERE registered_phone = $mobile_no");
    if ($results->registered_phone) {
        $opt = rand(1000, 9999);
        $upd['verify_otp'] = $opt;
        $wpdb->update('wp_delivery_boy', $upd, array('registered_phone' => $mobile_no));
        $data['Message'] = "OTP generated for verify mobile no.";
        $data['StatusCode'] = 200;
        $data['Result']['OTP'] = $opt;
    } else {
        $data['StatusCode'] = 400;
        $data['Message'] = "This number($mobile_no) is not registered.";
    }
    return rest_ensure_response($data);
}

function verifyOTP(WP_REST_Request $request) {
    global $wpdb;
    $mobile_no = $request['mobile_no'];
    $otp = $request['OTP'];
    $results = $wpdb->get_row("SELECT verify_otp FROM {$wpdb->prefix}delivery_boy WHERE registered_phone = $mobile_no");
    if ($results->verify_otp == $otp) {
        $data['Message'] = "OTP verified successfully.";
        $data['StatusCode'] = 200;
    } else {
        $data['Message'] = "OTP mismatched.";
        $data['StatusCode'] = 400;
    }
    return rest_ensure_response($data);
}

function login(WP_REST_Request $request) {
    global $wpdb;
    $mobile_no = $request['mobile_no'];
    $password = $request['password'];
    $imei_no = $request['IMEI_no'];
    if ($mobile_no && $password) {
        $results = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}delivery_boy WHERE "
                . "registered_phone = $mobile_no");
//        return rest_ensure_response(md5($password) . "+++" . $results->password);
        if (!empty($results)) {
            if ($results->status == 2) {
                $data['Message'] = "Inactive User";
                $data['StatusCode'] = 400;
                return rest_ensure_response($data);
            } else if (($mobile_no == $results->registered_phone) && (md5($password) == $results->password) && ($imei_no == $results->IMEI)) {
                $data['Message'] = "Logged in successfully.";
                $data['StatusCode'] = 200;
                $data['Result']['details'] = $results;
                return rest_ensure_response($data);
            } else {
                $data['Message'] = "Please provide correct password.";
                $data['StatusCode'] = 400;
                return rest_ensure_response($data);
            }
        }
    }
}

function getOrderList(WP_REST_Request $request) {
    global $wpdb;
    global $woocommerce;
    $deliveryboy_id = $request['deliveryboy_id'];
    if ($deliveryboy_id) {
        $assign_info = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}assign_order_deliveryboy WHERE "
                . "deliveryboy_id = $deliveryboy_id");
        $get_order_ids = $wpdb->get_results("SELECT order_id FROM {$wpdb->prefix}assign_order_deliveryboy WHERE "
                . "deliveryboy_id = $deliveryboy_id", OBJECT_K);
        $id_orders = array();
        foreach ($get_order_ids as $k => $v) {
            array_push($id_orders, $k);
        }
        //get order details
        /* foreach ($order_ids as $order_id) {
          // get an instance of the WC_Order object
          $order = new WC_Order($order_id);

          // The loop to get the order items which are WC_Order_Item_Product objects since WC 3+
          foreach ($order->get_items() as $item_id => $item_product) {
          //Get the product ID
          $item_product->get_product_id();
          //Get the WC_Product object
          array_push($item_product->get_product(), $result);
          }
          } */
        $get_all_orders = $get_all_items = array();

        //get countries object
        $countries_obj = new WC_Countries();
        $countries_array = $countries_obj->get_countries();
        $country_states_array = $countries_obj->get_states();
        //get countries object
        foreach ($id_orders as $order_id) {
            $order = new WC_Order($order_id);

            //get order details
            $order_data = $order->get_data(); // The Order data
            $order_info['id'] = $order_data['id'];
            $order_info['parent_id'] = $order_data['parent_id'];
            $order_info['status'] = $order_data['status'];
            $order_info['currency'] = $order_data['currency'];
            $order_info['price'] = $order->get_total();
            $order_info['prices_include_tax'] = $order_data['prices_include_tax'];
            $order_info['version'] = $order_data['version'];
            $order_info['payment_method_title'] = $order_data['payment_method_title'];
            $order_info['date_created'] = $order_data['date_created']->date('Y-m-d H:i:s');
            $order_info['date_modified'] = $order_data['date_modified']->date('Y-m-d H:i:s');
            $order_info['date_created'] = $order_data['date_created']->getTimestamp();
            $order_info['date_modified'] = $order_data['date_modified']->getTimestamp();
            $order_info['discount_total'] = $order_data['discount_total'];
            $order_info['discount_tax'] = $order_data['discount_tax'];
            $order_info['shipping_total'] = $order_data['shipping_total'];
            $order_info['shipping_tax'] = $order_data['shipping_tax'];
            $order_info['cart_tax'] = $order_data['cart_tax'];
            $order_info['total_tax'] = $order_data['total_tax'];
            $order_info['version'] = $order_data['version'];
            $order_info['version'] = $order_data['version'];

            //billing information
            $order_info['billing']['first_name'] = $order_data['billing']['first_name'];
            $order_info['billing']['last_name'] = $order_data['billing']['last_name'];
            $order_info['billing']['company'] = $order_data['billing']['company'];
            $order_info['billing']['address_1'] = $order_data['billing']['address_1'];
            $order_info['billing']['address_2'] = $order_data['billing']['address_2'];
            $order_info['billing']['city'] = $order_data['billing']['city'];
            $order_info['billing']['state'] = $country_states_array[$order_data['billing']['country']][$order_data['billing']['state']];
            $order_info['billing']['postcode'] = $order_data['billing']['postcode'];
            $order_info['billing']['country'] = $countries_array[$order_data['billing']['country']];
            $order_info['billing']['email'] = $order_data['billing']['email'];
            $order_info['billing']['phone'] = $order_data['billing']['phone'];

            //shipping information
            $order_info['shipping']['first_name'] = $order_data['shipping']['first_name'];
            $order_info['shipping']['last_name'] = $order_data['shipping']['last_name'];
            $order_info['shipping']['company'] = $order_data['shipping']['company'];
            $order_info['shipping']['address_1'] = $order_data['shipping']['address_1'];
            $order_info['shipping']['address_2'] = $order_data['shipping']['address_2'];
            $order_info['shipping']['city'] = $order_data['shipping']['city'];
            $order_info['shipping']['state'] = $country_states_array[$order_data['shipping']['country']][$order_data['shipping']['state']];
            $order_info['shipping']['postcode'] = $order_data['shipping']['postcode'];
            $order_info['shipping']['country'] = $countries_array[$order_data['shipping']['country']];

            $items = $order->get_items();
            foreach ($items as $item_data) {
                $item_info['order_id'] = $order_data['id'];
                $item_info['product_id'] = $item_data['product_id'];
                $item_info['name'] = $item_data['name'];
                $item_info['variation_id'] = $item_data['variation_id'];
                $item_info['quantity'] = $item_data['quantity'];
                $item_info['tax_class'] = $item_data['tax_class'];
                $item_info['subtotal'] = $item_data['subtotal'];
                $item_info['subtotal_tax'] = $item_data['subtotal_tax'];
                $item_info['total'] = $item_data['total'];
                $item_info['total_tax'] = $item_data['total_tax'];
                array_push($get_all_items, $item_info);
            }
            array_push($get_all_orders, $order_info);
        }

//        $order_details = $get_all_orders;
        $order_details['order_details'] = $get_all_orders;
        $order_details['item_details'] = $get_all_items;
        if (!empty($assign_info)) {
            $data['Message'] = "Order information found.";
            $data['StatusCode'] = 200;
            $data['assign_details'] = $assign_info;
            $data['order_item_details'] = $order_details;
        } else {
            $data['Message'] = "No Order found.";
            $data['StatusCode'] = 400;
        }

        return rest_ensure_response($data);
    }
}

function verifyDeliveryCode(WP_REST_Request $request) {
    global $wpdb;
    $verification_code = $request['verification_code'];
    $order_id = $request['order_id'];
    $results = $wpdb->get_row("SELECT verification_code FROM {$wpdb->prefix}assign_order_deliveryboy WHERE order_id = $order_id");
    if (!empty($results)) {
        if ($results->verification_code == $verification_code) {
            $data['Message'] = "Verification code is matched!";
            $data['StatusCode'] = 200;
        } else {
            $data['Message'] = "Verification code is mismatched!";
            $data['StatusCode'] = 400;
        }
    } else {
        $data['Message'] = "Order id is not available!";
        $data['StatusCode'] = 400;
    }
    return rest_ensure_response($data);
}

function updateOrderStatus(WP_REST_Request $request) {
    global $wpdb;
    $order_status = $request['order_status'];
    $order_id = $request['order_id'];
    $results = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}assign_order_deliveryboy WHERE order_id = $order_id");
    if (!empty($results)) {
        $order = new WC_Order($order_id);
        if (!empty($order)) {
            $order->update_status($order_status);
        }

        //update in delivery boy assign table
        if ($order_status == "cancelled") {
            $upd['status_description'] = $request['status_description'];
            $upd['delivery_status'] = 2;
        } else {
            $upd['delivery_status'] = 1;
        }
        if ($request['status_description']) {
            $upd['payment_received'] = $request['payment_received'];
        }
        $wpdb->update('wp_assign_order_deliveryboy', $upd, array('order_id' => $order_id));
        $data['Message'] = "Order status updated successfully!";
        $data['StatusCode'] = 200;
    } else {
        $data['Message'] = "Order id is unavailable!";
        $data['StatusCode'] = 400;
    }
    return rest_ensure_response($data);
}

function regenerateVerificationCode(WP_REST_Request $request) {
    global $wpdb;
    $assign_id = $request['assign_id'];
    $order_id = $request['order_id'];

    $order = new WC_Order($order_id);
    $customer_mail = $order->get_billing_email();

    //update venrification code in db
    $upd['verification_code'] = rand(1000, 9999);
    $wpdb->update('wp_assign_order_deliveryboy', $upd, array('assign_id' => $assign_id));

    //send code to customer
    define("HTML_EMAIL_HEADERS", array('Content-Type: text/html; charset=UTF-8'));
    $mailer = WC()->mailer();
    $message = 'Hello ' . $order->billing_first_name . ' ' . $order->billing_last_name . ','
            . '<br/><br/>'
            . 'Your new verification code for order confirmation is <strong>' . $upd['verification_code'] . '</strong>.';
    $wrapped_message = $mailer->wrap_message($heading = 'New Verification Code : #' . $order_id . '', $message);
    $wc_email = new WC_Email;
    $html_message = $wc_email->style_inline($wrapped_message);
    wp_mail($customer_mail, "New Verification Code", $html_message, HTML_EMAIL_HEADERS);

    $data['Message'] = "Verification code re-generated successfully!";
    $data['StatusCode'] = 200;
    return rest_ensure_response($data);
}

add_action('rest_api_init', 'my_register_route');
