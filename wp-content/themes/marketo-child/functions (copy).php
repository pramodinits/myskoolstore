<?php
/* ---------------------------------------------------
 * Theme: Charitious - WordPress Theme
 * Author: XpeedStudio
 * Author URI: http://www.xpeedstudio.com
  -------------------------------------------------- */
global $wpdb;

function marketo_theme_enqueue_styles() {

    $parent_style = 'parent-style';

    if (!wp_style_is($parent_style, $list = 'enqueued')) {
        wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css', array());
    }
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style)
    );
    wp_enqueue_script('child-custom', get_stylesheet_directory_uri() . '/custom.js', array('jquery'), '', true);
}

add_action('wp_enqueue_scripts', 'marketo_theme_enqueue_styles', 99);

function my_login_logo_one() {
    ?> 
    <style type="text/css"> 
        body.login div#login h1 a {
            background-image: url(http://localhost/mySkoolStore/wp-content/uploads/2020/05/Logo_1.png); 
            width: 100%;
            background-size: 150px;
            padding-bottom: 10px;
        } 
    </style>
    <?php
}

add_filter('pre_get_posts', 'custom_pre_get_posts');

function custom_pre_get_posts($query) {
    if (is_search()) {
        $query->set('post_type', 'product');
    }

    return $query;
}

add_action('login_enqueue_scripts', 'my_login_logo_one');

//add custom menu
add_action('admin_menu', 'custom_menu');

function custom_menu() {
//  add_menu_page( 
//      'City', 
//      'Manage City', 
//      'edit_posts', 
//      'manage_city', 
//      'manage_city.php', 
//      'dashicons-location',
//        3
//     );
    add_menu_page(
            __('Manage City', 'textdomain'), 'Manage City', 'manage_options', 'manage_city.php', '', 'dashicons-location', 3
    );
    add_menu_page(
            __('Manage School', 'textdomain'), 'Manage School', 'manage_options', 'manage_school.php', '', 'dashicons-store', 3
    );
}

//add new table

/**
 * Get AJAX school
 */
if (isset($_POST['city_id'])) {
    global $wpdb;
    $city_id = $_POST['city_id'];
    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id", OBJECT_K);
    $result_school = array();
    $str = "<option value=''>Select School</option>";
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }
    foreach ($result_school as $key => $val) {
        $str .= "<option value='{$key}'>" . $val . "</option>";
    }
    echo $str;
    exit;
}

if (isset($_POST['city_id_edit'])) {
    global $wpdb;
    $city_id = $_POST['city_id_edit'];

    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id", OBJECT_K);
    $result_school = array();
    $str = "<option value=''>Select School</option>";
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }
//    $reg_schools = implode(',', array_keys($school_list));
//    echo $reg_schools; exit;
    foreach ($result_school as $key => $val) {
        $str .= "<option value='{$key}'>" . $val . "</option>";
    }
    echo $str;
    exit;
}

/**
 * Get city lists
 */
//add new custom fields
function add_distributor_fields($current_user, $profile_info) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'school';
    $table_name_city = $wpdb->prefix . 'city';

    $city_list = $wpdb->get_results("SELECT city_id, city_name FROM $table_name_city", OBJECT_K);
    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM $table_name WHERE school_id = $user->vendor_school", OBJECT_K);
    $result_city = $result_school = array();
    foreach ($city_list as $k => $v) {
        $result_city[$k] = $v->city_name;
    }
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }

    $user = get_user_by('id', get_current_user_id());
    ?>      
    <div class="gregcustom dokan-form-group">
        <label class="dokan-w3 dokan-control-label" for="setting_address"><?php _e('Distributor City', 'dokan'); ?></label>
        <div class="dokan-w5">
            <select class="dokan-form-control" id="vendor-city-edit" name="vendor_city" required="">
                <option value="">Select City</option>
                <?php foreach ($result_city as $k => $v) { ?>
                    <option value="<?= $k ?>" <?= $k == $user->vendor_city ? "selected" : "" ?>><?= $v ?></option>
                <?php }
                ?>
            </select>
        </div>
    </div>  

    <div class="gregcustom dokan-form-group">
        <label class="dokan-w3 dokan-control-label" for="setting_address"><?php _e('Distributor School', 'dokan'); ?></label>
        <div class="dokan-w5">
            <select class="dokan-form-control" id="vendor-school-edit" name="vendor_school" required="">
                <option value="">Select School</option>
                <?php foreach ($result_school as $k => $v) { ?>
                    <option value="<?= $k ?>" <?= $k == $user->vendor_school ? "selected" : "" ?>><?= $v ?></option>
                <?php }
                ?>
            </select>
        </div>
    </div>
    <?php
}

add_filter('dokan_settings_after_banner', 'add_distributor_fields', 10, 2);

/**
 * Save the extra fields.
 *
 * @param  int  $customer_id Current customer ID.
 *
 * @return void
 */
function save_extra_vendor_fields($store_id, $dokan_settings) {
    $post_data = wp_unslash($_POST);
    
//    update_user_meta($store_id, 'dokan_profile_settings', $dokan_settings);
    update_user_meta($store_id, 'vendor_city', sanitize_text_field($post_data['vendor_city']));
    update_user_meta($store_id, 'vendor_school', sanitize_text_field($post_data['vendor_school']));
}

add_action('dokan_store_profile_saved', 'save_extra_vendor_fields', 10, 2);

/**
 * Set field as a required one
 *
 * @since 1.0.0
 *
 * @param array $required_fields
 *
 * @return array
 */
function dokan_custom_seller_registration_required_fields($required_fields) {
    $required_fields['vendor_city'] = __('Please enter selling city', 'dokan-custom');
    $required_fields['vendor_school'] = __('Please enter selling school', 'dokan-custom');

    return $required_fields;
}

;
add_filter('dokan_seller_registration_required_fields', 'dokan_custom_seller_registration_required_fields');

/**
 * Add field in the registration form
 *
 */
function dokan_custom_seller_registration_field_after() {
    $post_data = wp_unslash($_POST);

    /**
     * Get city lists
     */
    global $wpdb;
    $table_name = $wpdb->prefix . 'school';
    $table_name_city = $wpdb->prefix . 'city';

    $city_list = $wpdb->get_results("SELECT city_id, city_name FROM $table_name_city", OBJECT_K);
    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM $table_name", OBJECT_K);
    $result_city = $result_school = array();
    foreach ($city_list as $k => $v) {
        $result_city[$k] = $v->city_name;
    }
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }
    ?>
    <p class="form-row form-group form-row-wide">
        <label for="vendor-city"><?php esc_html_e('Distributor City', 'dokan-custom'); ?><span class="required">*</span></label>
        <select class="form-control" id="vendor-city-reg" name="vendor_city" required="">
            <option value="">Select City</option>
            <?php foreach ($result_city as $k => $v) { ?>
                <option value="<?= $k ?>"><?= $v ?></option>
            <?php }
            ?>
        </select>
    </p>
    <p class="form-row form-group form-row-wide">
        <label for="vendor-school"><?php esc_html_e('Distributor School', 'dokan-custom'); ?><span class="required">*</span></label>
        <select class="form-control" id="vendor-school-reg" name="vendor_school" required="">
            <option value="">Select School</option>
        </select>
    </p>
    <?php
}

/**
 * Override the global/seller-registration-form template and add new field
 *
 * @since 1.0.0
 *
 * @param string $template
 * @param string $slug
 * @param string $name
 *
 * @return string
 */
//function dokan_custom_get_template_part($template, $slug, $name) {
//    if ('global/seller-registration-form' === $slug) {
//        $template = dirname(__FILE__) . '/dokan-seller-registration-form.php';
//    }
//
//    return $template;
//}

add_filter('dokan_seller_registration_field_after', 'dokan_custom_seller_registration_field_after');
?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#vendor-city-reg').change(function () {
            var selected_val = $("#vendor-city-reg").val();
            $.ajax(
                    {
                        type: 'post',
                        data: {city_id: selected_val},
                        success: function (res) {
                            $("#vendor-school-reg").html(res);
                        }

                    });
        });
        $('#vendor-city-edit').change(function () {
            var selected_val = $("#vendor-city-edit").val();
            $.ajax(
                    {
                        type: 'post',
                        data: {city_id_edit: selected_val},
                        success: function (res) {
                            $("#vendor-school-edit").html(res);
                        }

                    });
        });
    });
</script>

<?php

/**
 * Save custom field data
 *
 * @since 1.0.0
 *
 * @param int   $vendor_id
 * @param array $dokan_settings
 *
 * @return void
 */
function dokan_custom_new_seller_created($vendor_id, $dokan_settings) {
    $post_data = wp_unslash($_POST);

    $vendor_city = sanitize_text_field($post_data['vendor_city']);
    $vendor_school = sanitize_text_field($post_data['vendor_school']);

    /**
     * This will save agent_id value with the `dokan_custom_agent_id` user meta key
     */
    update_user_meta($vendor_id, 'vendor_city', $vendor_city);
    update_user_meta($vendor_id, 'vendor_school', $vendor_school);
}

add_action('dokan_new_seller_created', 'dokan_custom_new_seller_created', 10, 2);

function get_store_url($atts) {

    $a = shortcode_atts(
            array(
        'seller_ids' => '',
            ), $atts);
    return $a;
}

//add_shortcode( 'dokan-stores', 'get_store_url' );
//[dokan-stores  seller_ids = "4,5"]