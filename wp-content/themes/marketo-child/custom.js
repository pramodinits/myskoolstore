$(document).ready(function () {

    $('#vendor-city-edit').change(function () {
        var selected_val = $("#vendor-city-edit").val();
        var userid = $("#userid").val();
        $.ajax({type: 'post',
            url: 'functions.php',
            data: {city_id_edit: selected_val, userid: userid},
            success: function (res) {
                $("#vendor-school-edit").html(res);
            }
        });
    })
});

function getMoreSchool(e) {
    var selected_val = e.value;
    var select_parent_id = e.parentNode.id;
    var div_id = $("#"+select_parent_id).next().find('select').attr('id');
    //get selected schools in array
    var selectedArray=[]; 
     $('select[name="vendor_school[]"] option:selected').each(function() {
      selectedArray.push($(this).val());
     });
     console.log(selectedArray);
     //get dynamic schools
     $.ajax({type: 'post',
            url: 'functions.php',
            data: {city_id_edit: selected_val, selectedSchools: selectedArray},
            success: function (res) {
//                console.log(res);
                $("#"+div_id).html(res);
            }
        });
}

function getSchool(e) {
    var selected_val = e.value;
//    console.log(e);
    var select_parent_id = e.parentNode.id;
//    var div_id = $("#" + select_parent_id).next(".flow-content").attr("data-key");
            var div_id = $("#"+select_parent_id).next().find('.magicsearch').attr('id');
//    console.log(div_id);
    $.ajax({
                type: 'post',
            url: 'functions.php',
                data: {city_id_edit: selected_val},
                success: function (res) {
                    var obj = jQuery.parseJSON(res);
                    var dataSource = [];
                        $.each(obj, function (key, value) {
                        dataSource.push({id: key, locationName: value},);
                    });
                    $('#'+div_id).magicsearch({
                        dataSource: dataSource,
                fields: ['locationName'],
                id: 'id',
                format: '%locationName%',
                multiple: true,
                multiField: 'locationName',
                dropdownBtn: true,
                multiStyle: {
                    space: 5,
                    width: 100
                },
                success: function ($input, data) {
                    $("#school_ids").val($('#basic').attr('data-id'));
                }
                    });
                }

            });
}

function changestatus(id_deliveryboy, status) {
    var status_val = (status == 1) ? 2 : 1;
    if (status_val == 2) {
        var title = "Click to Active";
        var msg = "Deactivated";
        var clss = "dokan-label dokan-label-danger";
        var showtitle = "In-Active";
    } else {
        var title = "Click to In-Active";
        var msg = "Activated";
        var clss = "dokan-label dokan-label-success";
        var showtitle = "Active";
    }
    var conf = confirm("Are you sure to change the status");
    if (conf) {
//            var url = "<?= base_url('admin/changeStatus/'); ?>";
        $.post('', {"id": id_deliveryboy, "status_val": status_val, "tbl_name": 'wp_delivery_boy', "fld_name": 'delivery_boy_id'}, function (res) {
            $("#show_status").show().addClass("btn-success").html("Delivery Boy" + msg + " successfully.");
            var icon = "<span class='" + clss + "' title='" + title + "' data-toggle='tooltip'>" + showtitle + "</span>";
            $("#status_row_" + id_deliveryboy).html('<a href="javascript:void(0);" onclick="changestatus(\'' + id_deliveryboy + '\',\'' + status_val + '\');">' + icon + '</a>');
            $("html, body").animate({scrollTop: 0}, "slow");
            setTimeout(function () {
                $("#show_status").hide();
            }, 4000);
        });
        return true;
    } else {
        return false;
    }
}