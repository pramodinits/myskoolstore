<?php
/**
 *  Dokan Dashboard Add Delivery boy Template
 *
 *  Dokan Main Dahsboard template for Front-end
 *
 *  @since 2.4
 *
 *  @package dokan
 */
$get_data = wp_unslash($_GET);
$id_delivery_id = $get_data['id'];
global $wpdb;
if ($id_delivery_id) {
    $info = $wpdb->get_row("SELECT * FROM wp_delivery_boy WHERE delivery_boy_id = $id_delivery_id");
}
?>

<?php do_action('dokan_dashboard_wrap_start'); ?>

<div class="dokan-dashboard-wrap">

    <?php
    /**
     *  dokan_dashboard_content_before hook
     *  dokan_before_new_product_content_area hook
     *
     *  @hooked get_dashboard_side_navigation
     *
     *  @since 2.4
     */
    do_action('dokan_dashboard_content_before');
    do_action('dokan_before_new_deliveryboy_content_area');
    ?>

    <div class="dokan-dashboard-content">
        <?php
        /**
         *  dokan_before_new_product_inside_content_area hook
         *
         *  @since 2.4
         */
        do_action('dokan_before_new_deliveryboy_inside_content_area');
        ?>

        <header class="dokan-dashboard-header">
            <h1 class="entry-title">
                <?php esc_html_e('Add New Delivery Boy', 'dokan-lite'); ?>
            </h1>
        </header><!-- .entry-header -->


        <?php dokan_deliveryboy_dashboard_errors(); 
//        $formdata =  $_SESSION['formdata'];
        ?>

        <?php if (dokan_is_seller_enabled(get_current_user_id())) { ?>
            <form class="dokan-form-horizontal" method="post" action="<?= dokan_get_navigation_url('action') ?>">
                <input type="hidden" name="delivery_boy_id" value="<?= @$info->delivery_boy_id ?>"/>
                <div class="dokan-form-group">
                    <label class="dokan-w3 dokan-control-label" for="title"><?php _e('First Name', 'dokan'); ?><span class="required"> *</span></label>
                    <div class="dokan-w5 dokan-text-left">
                        <?php
                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            $val = isset($_POST['first_name']) ? htmlspecialchars($_POST['first_name'], ENT_QUOTES) : '';
                        } elseif (@$info->first_name) {
                            $val = @$info->first_name;
                        }
                        ?>
                        <input id="first_name" name="first_name" required value="<?= @$val ?>" placeholder="<?php _e('First Name', 'dokan'); ?>" class="dokan-form-control input-md" type="text">
                    </div>
                </div>
                <div class="dokan-form-group">
                    <label class="dokan-w3 dokan-control-label" for="title"><?php _e('Last Name', 'dokan'); ?><span class="required"> *</span></label>
                    <div class="dokan-w5 dokan-text-left">
                        <input id="last_name" name="last_name" required value="<?= @$info->last_name ?>" placeholder="<?php _e('Last Name', 'dokan'); ?>" class="dokan-form-control input-md" type="text">
                    </div>
                </div>
                <div class="dokan-form-group">
                    <label class="dokan-w3 dokan-control-label" for="title"><?php _e('Registered Phone', 'dokan'); ?><span class="required"> *</span></label>
                    <div class="dokan-w5 dokan-text-left">
                        <input id="registered_phone" name="registered_phone" required value="<?= @$info->registered_phone ?>" placeholder="<?php _e('Registered Phone', 'dokan'); ?>" class="dokan-form-control input-md" type="text">
                    </div>
                </div>
                <div class="dokan-form-group">
                    <label class="dokan-w3 dokan-control-label" for="title"><?php _e('Other Phone', 'dokan'); ?></label>
                    <div class="dokan-w5 dokan-text-left">
                        <input id="other_phone" name="other_phone" value="<?= @$info->other_phone ?>" placeholder="<?php _e('Other Phone', 'dokan'); ?>" class="dokan-form-control input-md" type="text">
                    </div>
                </div>
                <div class="dokan-form-group">
                    <label class="dokan-w3 dokan-control-label" for="title"><?php _e('Email', 'dokan'); ?><span class="required"> *</span></label>
                    <div class="dokan-w5 dokan-text-left">
                        <input id="email" name="email" required value="<?= @$info->email ?>" placeholder="<?php _e('Email', 'dokan'); ?>" class="dokan-form-control input-md" type="text">
                    </div>
                </div>
                <div class="dokan-form-group">
                    <div class="dokan-w5 ajax_prev dokan-text-left" style="margin-left:23%">
                        <?php if (@$info->delivery_boy_id) { ?>
                            <input type="submit" id="" name="deliveryboy_update" value="Update" class="dokan-btn dokan-btn-danger dokan-btn-theme">
                        <?php } else { ?>
                            <input type="submit" id="" name="deliveryboy_creation" value="Submit" class="dokan-btn dokan-btn-danger dokan-btn-theme">
                        <?php } ?>
                    </div>
                </div>
            </form>
        <?php }
        ?>
    </div>
</div>