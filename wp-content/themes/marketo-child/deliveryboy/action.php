<?php

if (!is_user_logged_in()) {
    return;
}

if (!dokan_is_user_seller(get_current_user_id())) {
    return;
}

global $wpdb;
$table_name = $wpdb->prefix . 'delivery_boy';

$postdata = wp_unslash($_POST);
//print_r($postdata);exit;
if (isset($postdata['deliveryboy_creation']) || isset($postdata['deliveryboy_update'])) {
    $first_name = sanitize_text_field($postdata['first_name']);
    $last_name = sanitize_text_field($postdata['last_name']);
    $registered_phone = sanitize_text_field($postdata['registered_phone']);
    $other_phone = sanitize_text_field($postdata['other_phone']);
    $email = sanitize_text_field($postdata['email']);
    $id = $postdata['delivery_boy_id'];
    $post_data = apply_filters('dokan_insert_product_post_data', array(
        'first_name' => $first_name,
        'last_name' => $last_name,
        'registered_phone' => $registered_phone,
        'other_phone' => $other_phone,
        'email' => $email,
        'vendor_id' => get_current_user_id(),
    ));
//    print_r($post_data);exit;
    if ($id) {
        $wpdb->update($table_name, $post_data, array('delivery_boy_id' => $id));
        wp_redirect(add_query_arg(array('message' => 'deliveryboy_created'), dokan_get_navigation_url('deliveryboy')));
        exit;
    } else {
        //check if phone exists
        $results = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}delivery_boy WHERE registered_phone = $registered_phone");
        if ($results->registered_phone) {
//            $_SESSION['formdata'] = $post_data;
            wp_redirect(add_query_arg(array('message' => 'deliveryboy_exists'), dokan_get_navigation_url('add-deliveryboy')));
            exit;
        } else {
            if ($wpdb->insert($table_name, $post_data)) {
                /*
                 * Mail to delivery boy
                 */
                define("HTML_EMAIL_HEADERS", array('Content-Type: text/html; charset=UTF-8'));
                $mailer = WC()->mailer();
                $message = 'Hello ' . $first_name . ' '. $last_name. ','
                        . '<br/><br/>'
                        . 'You have been registered as delivery boy on the mobile no <strong>' .$registered_phone. '</strong>.'
                        . '<br/><br/>'
                        . 'Kindly use this registered number in mobile application usage.';
                $wrapped_message = $mailer->wrap_message($heading = 'Registered as Delivery Boy', $message);
                $wc_email = new WC_Email;
                $html_message = $wc_email->style_inline($wrapped_message);
                wp_mail($_POST['billingemail'], "Delivery Boy Registration", $html_message, HTML_EMAIL_HEADERS);
                
                wp_redirect(add_query_arg(array('message' => 'deliveryboy_created'), dokan_get_navigation_url('deliveryboy')));
                exit;
            } else {
                $redirect = dokan_get_navigation_url('add-deliveryboy');
                wp_redirect(add_query_arg(array('message' => 'error'), $redirect));
            }
        }
    }
//    $product_id = $wpdb->insert( $table_name , $post_data );
}