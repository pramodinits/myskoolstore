<?php
/**
 *  Dokan Dashboard Delivery boy Template
 *
 *  Dokan Main Dahsboard template for Front-end
 *
 *  @since 2.4
 *
 *  @package dokan
 */

//change status
if (isset($_POST['id_deliveryboy'])) {
    global $wpdb;
    $id_deliveryboy = $_POST['id'];
    $status = $_POST['status_val'];
    $tbl_name = $_POST['tbl_name'];
    $fld_name = $_POST['fld_name'];
    $wpdb->query($wpdb->prepare("UPDATE $tbl_name SET status='$status' WHERE $fld_name=$id_deliveryboy"));
    echo "success";
    exit();
}
wp_enqueue_script('child-custom', get_stylesheet_directory_uri() . '/custom.js', array('jquery'), '', true);
?>

<?php do_action('dokan_dashboard_wrap_start');
?>

<div class="dokan-dashboard-wrap">
    
    <?php
    /**
     *  dokan_dashboard_content_before hook
     *
     *  @hooked get_dashboard_side_navigation
     *
     *  @since 2.4
     */
    do_action('dokan_dashboard_content_before');
    ?>

    <div class="dokan-dashboard-content">

        <?php
        /**
         *  dokan_dashboard_content_before hook
         *
         *  @hooked show_seller_dashboard_notice
         *
         *  @since 2.4
         */
        do_action('dokan_help_content_inside_before');
        do_action('dokan_before_listing_deliveryboy');
        ?>

        <article class="dokan-deliveryboy-listing-area">

            <div class="deliveryboy-listing-top dokan-clearfix">
                <ul class="list-inline order-statuses-filter">


                </ul>
                <?php if (dokan_is_seller_enabled(get_current_user_id())): ?>
                    <span class="dokan-add-deliveryboy-link">
                        <a href="<?php echo esc_url(dokan_get_navigation_url('add-deliveryboy')); ?>" class="dokan-btn dokan-btn-theme">
                            <i class="fa fa-user">&nbsp;</i>
                            <?php esc_html_e('Add Delivery Boy', 'dokan-lite'); ?>
                        </a>
                        <?php
                        do_action('dokan_after_add_deliveryboy_btn');
                        ?>
                    </span>
                <?php endif; ?>
            </div>

            <?php dokan_deliveryboy_dashboard_errors(); ?>

            <!--listing filter-->
            <div class="dokan-alert dokan-alert-success" id="show_status" style="display: none;">

            </div>
            <!--listing filter-->

            <!--delivery boy listing-->
            <div class="dokan-dashboard-deliveryboy-listing-wrapper dokan-clearfix">
                <table class="dokan-table">
                    <thead>
                        <tr>
                            <th><?php _e('Full Name', 'dokan'); ?></th>
                            <th><?php _e('Registered Phone', 'dokan'); ?></th>
                            <th><?php _e('Other Phone', 'dokan'); ?></th>
                            <th><?php _e('Email', 'dokan'); ?></th>
                            <th><?php _e('Status', 'dokan'); ?></th>
                            <th><?php _e('Registered Date', 'dokan'); ?></th>
                            <th><?php _e('Action', 'dokan'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        global $wpdb;
                        /* $paged = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;
                          $post_per_page = intval(get_query_var('posts_per_page'));
                          $offset = ($paged - 1) * $post_per_page;
                         */
                        $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;
                        $limit = 10;
                        $offset = ( $pagenum - 1 ) * $limit;

                        $total_rows = $wpdb->get_var("SELECT COUNT(delivery_boy_id) FROM wp_delivery_boy WHERE vendor_id = ". get_current_user_id());
                        $num_of_pages = ceil($total_rows / $limit);

                        $total_records = $wpdb->get_results("SELECT * FROM wp_delivery_boy WHERE vendor_id = ". get_current_user_id() . " LIMIT $offset, $limit");
//                        print $wpdb->last_query;
                        $rowcount = $wpdb->num_rows;
                        
                        if($total_records) {
                        foreach ($total_records as $records) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo esc_attr($records->first_name) . " " . esc_attr($records->last_name); ?>
                                </td>
                                <td>
                                    <?php echo esc_attr($records->registered_phone); ?>
                                </td>
                                <td>
                                    <?= $records->other_phone ? esc_attr($records->other_phone) : "N/A"; ?>
                                </td>
                                <td>
                                    <?php echo esc_attr($records->email); ?>
                                </td>
                                <td id="status_row_<?= $records->delivery_boy_id; ?>">
                                    <?php if ($records->status == 1) {
                                        $show_status = '<span class="dokan-label dokan-label-success" data-toggle="tooltip" title="Click to In-Active">Active</span>';
                                         } elseif ($records->status == 2) {
                                        $show_status = '<span class="dokan-label dokan-label-danger" data-toggle="tooltip" title="Click to Active">In-Active</span>'; 
                                     }
                                    ?>
                                    <a href='javascript:void(0);' onclick='changestatus(<?= $records->delivery_boy_id; ?>,<?= $records->status; ?>);'><?= $show_status; ?></a>
                                </td>
                                <td>
                                    <?php echo date('M j, Y', strtotime(esc_attr($records->registered_date))); ?>
                                </td>
                                <td class="dokan-order-action">
                                    <a href="<?= dokan_get_navigation_url('add-deliveryboy') . '?id=' . $records->delivery_boy_id ?>" class="dokan-btn dokan-btn-default dokan-btn-sm tips">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php }
                        } else { ?>
                            <tr>
                                <td colspan="7">
                                        <?php esc_html_e('No delivery boy found', 'dokan-lite'); ?>
                                </td>
                            </tr>
<!--                    <div class="dokan-error">
                            <?php esc_html_e('No delivery boy found', 'dokan-lite'); ?>
                        </div>-->
                        <?php }
                        ?>
                    </tbody>
                </table>
                <?php
                $page_links = paginate_links(array(
                    'base' => add_query_arg('pagenum', '%#%'),
                    'format' => '',
                    'prev_text' => __('&laquo;', 'aag'),
                    'next_text' => __('&raquo;', 'aag'),
                    'total' => $num_of_pages,
                    'current' => $pagenum
                ));

                if ($page_links) {
                    echo "<ul class='pagination'>\n\t<li>";
                    echo $page_links;
                    echo "</li>\n</ul>\n";
                }
                ?>
            </div>
            <!--delivery boy listing-->
            <div class="clearfix"></div>
        </article><!-- .dashboard-content-area -->
        <div class="dokan-clearfix"></div>
        <?php
        /**
         *  dokan_dashboard_content_inside_after hook
         *
         *  @since 2.4
         */
        do_action('dokan_dashboard_content_inside_after');
        ?>


    </div><!-- .dokan-dashboard-content -->

    <?php
    /**
     *  dokan_dashboard_content_after hook
     *
     *  @since 2.4
     */
    do_action('dokan_dashboard_content_after');
    ?>

</div><!-- .dokan-dashboard-wrap -->
<?php do_action('dokan_dashboard_wrap_end'); ?>



