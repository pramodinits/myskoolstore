<?php
/**
 * template-full-width.php
 *
 * Template Name: Select Option Page
 */

//get school lists
if (isset($_POST['city_id_store'])) {
    global $wpdb;
    $city_id = $_POST['city_id_store'];
    $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school WHERE id_city = $city_id", OBJECT_K);
    $result_school = array();
    $str = "<option value=''>Select School</option>";
    foreach ($school_list as $k => $v) {
        $result_school[$k] = $v->school_name;
    }
    foreach ($result_school as $key => $val) {
        $str .= "<option value='{$key}'>" . $val . "</option>";
    }
    echo $str;
    exit;
}

?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#buyer_city').change(function () {
            var selected_val = $("#buyer_city").val();
            $.ajax({type: 'post',
                data: {city_id_store: selected_val},
                success: function (res) {
                    $("#buyer_school").html(res);
                }
            });
        });
    });
</script>
<?php
get_header();
get_template_part('template-parts/header/content', 'page-header');
session_start();
if(isset($_SESSION['vendor_id'])) {
    session_destroy ();
}
?>


<?php
global $wpdb;
$table_name_school = $wpdb->prefix . 'school';
$table_name_city = $wpdb->prefix . 'city';

$city_list = $wpdb->get_results("SELECT city_id, city_name FROM $table_name_city", OBJECT_K);
$result_city = $result_school = array();
foreach ($city_list as $k => $v) {
    $result_city[$k] = $v->city_name;
}



//make search results
$get_data = wp_unslash($_GET);
if (isset($get_data['search_products'])) {
    if (isset($get_data['buyer_city'])) {
        $city_id = $get_data['buyer_city'];
    }
    if (isset($get_data['buyer_school'])) {
        $school_id = $get_data['buyer_school'];
        $school_list = $wpdb->get_results("SELECT school_id, school_name FROM wp_school"
                . " WHERE id_city='$city_id'", OBJECT_K);
        $result_school_select = array();
        foreach ($school_list as $k => $v) {
            $result_school_select[$k] = $v->school_name;
        }
    }
//    $city_id = $_POST['buyer_city'];
//    $school_id = $_POST['buyer_school'];

    $table_usermeta = $wpdb->prefix . 'usermeta';
    $table_posts = $wpdb->prefix . 'posts';

//$cond = "meta_key = 'vendor_city' and meta_value = $city_id OR meta_key = 'vendor_school' and meta_value = $school_id";
    $cond = "meta_key = 'vendor_school_ids' and FIND_IN_SET($school_id,meta_value)";
    $get_vendors = $wpdb->get_results("SELECT user_id FROM $table_usermeta where $cond", OBJECT_K);
    $id_vendors = array();
    foreach ($get_vendors as $k => $v) {
        array_push($id_vendors, $k);
    }
    $ids = implode(",", $id_vendors);
    $_SESSION['distributors_search_ids'] = $ids; //to implement in search
    
    //To redirect specific store
    if ($ids) {
        $_SESSION['vendor_id'] = $ids;
        $store_url = dokan_get_store_url($ids);
        $shop_page_url = get_permalink(woocommerce_get_page_id('shop'));

        echo "<script>location.replace('$shop_page_url')</script>";
    }

    //To get product lists
     $get_products = $wpdb->get_results("SELECT * FROM $table_posts WHERE post_author IN ($ids)", OBJECT_K);

      $id_products = array();
      foreach ($get_products as $k => $v) {
      array_push($id_products, $k);
      }
      $product_ids = implode(",", $id_products);
}
?>
<style>
    .xs-product-wraper .version-2 img {
        padding: 0 22px !important;
    }
    .xs-product-header.media.xs-wishlist {
        height: 10px !important;
    }
    .xs-product-wraper.version-2 .xs-product-content {
        padding:  20px !important;
    }
    .page {
        min-height: 350px;
    }
    #dokan-store-listing-filter-form-wrap {
        display: none !important;
    }
</style>
<div class="page" role="main">
    <?php
//    echo do_shortcode('[woocommerce_product_search show_description="no" limit="20"]');
//    echo do_shortcode('[widget id="text-2"]');
    the_content();
    ?>
    <div class="builder-content xs-transparent">
        <div class="dokan-dashboard-wrap">
            <div class="dokan-dashboard-content">
                <article class="dashboard-content-area woocommerce edit-account-wrap">
                    <h1 class="entry-title text-center"><?php esc_html_e('Search Stores', 'dokan-lite'); ?></h1>
                    <form class="edit-account" action="" method="get">
                        <p class="form-row form-row-first">
                            <label for="buyer_city"><?php esc_html_e('City', 'dokan-lite'); ?> <span class="required">*</span></label>
                            <select class="dokan-form-control" id="buyer_city" name="buyer_city" required="">
                                <option value="">Select City</option>
                                <?php foreach ($result_city as $k => $v) { ?>
                                    <option value="<?= $k ?>" <?= $_GET['buyer_city'] == $k ? "selected" : "" ?>><?= $v ?></option>
                                <?php }
                                ?>
                            </select>
                        </p>
                        <p class="form-row form-row-last">
                            <label for="buyer_school"><?php esc_html_e('School', 'dokan-lite'); ?> <span class="required">*</span></label>
                            <select class="dokan-form-control" id="buyer_school" name="buyer_school" required="">
                                <option value="">Select School</option>
                                <?php foreach ($result_school_select as $k => $v) { ?>
                                    <option value="<?= $k ?>" <?= $_GET['buyer_school'] == $k ? "selected" : "" ?>><?= $v ?></option>
                                <?php }
                                ?>
                            </select>
                        </p>
                        <div class="clear"></div>
                        <p>
                            <input type="submit" class="dokan-btn dokan-btn-primary dokan-btn-theme" name="search_products" value="<?php esc_attr_e('Get Stores', 'dokan-lite'); ?>" />
                        </p>
                    </form>
                </article>
            </div>
        </div>
    </div> <!-- end main-content -->

    <div class="elementor-container elementor-column-gap-default" style="display: none;">
        <div class="elementor-row">
            <div class="tab-content xs-woo-tab">
                <div class="row feature-product-v4 woocommerce">
                    <div class="tab-content">

                        <?php if ($_GET['buyer_city']) {
                         if ($ids) {
                            echo do_shortcode('[dokan-stores seller_ids="' . $ids . '"]');
                            } else {
                                echo "<b>No stores found.</b>";
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--end main-content -->

<?php get_footer();
?>