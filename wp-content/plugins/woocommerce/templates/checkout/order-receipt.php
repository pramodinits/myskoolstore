<?php
/**
 * Checkout Order Receipt Template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/order-receipt.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.2.0
 */
if (!defined('ABSPATH')) {
    exit;
}

//store order meta
/*
global $wpdb;
if (isset($_SESSION['cartDetails'])) {
    $amount_key = $wpdb->get_row("SELECT meta_id FROM $wpdb->postmeta WHERE (meta_key = '_order_total') AND post_id=" . $order->get_id() . "");
    if ($amount_key->meta_id) {
        $wpdb->update($wpdb->postmeta, array('meta_value' => $_SESSION['cartDetails']['totalAmount']), array('meta_id' => $amount_key->meta_id));
    } else {
        $post_data = array(
            'post_id' => $order->get_id(),
            'meta_key' => '_order_total',
            'meta_value' => $_SESSION['cartDetails']['totalAmount']
        );
        $wpdb->insert($wpdb->postmeta, $post_data);
    }
    $taxlabel_key = $wpdb->get_row("SELECT meta_id FROM $wpdb->postmeta WHERE (meta_key = '_order_tax_label') AND post_id=" . $order->get_id() . "");
    if ($taxlabel_key->meta_id) {
        $wpdb->update($wpdb->postmeta, array('meta_value' => $_SESSION['cartDetails']['taxLabel']), array('meta_id' => $taxlabel_key->meta_id));
    } else {
        $post_data = array(
            'post_id' => $order->get_id(),
            'meta_key' => '_order_tax_label',
            'meta_value' => $_SESSION['cartDetails']['taxLabel']
        );
        $wpdb->insert($wpdb->postmeta, $post_data);
    }
    $taxamount_key = $wpdb->get_row("SELECT meta_id FROM $wpdb->postmeta WHERE (meta_key = '_order_tax') AND post_id=" . $order->get_id() . "");
    if ($taxamount_key->meta_id) {
        $wpdb->update($wpdb->postmeta, array('meta_value' => $_SESSION['cartDetails']['taxAmount']), array('meta_id' => $taxamount_key->meta_id));
    } else {
        $post_data = array(
            'post_id' => $order->get_id(),
            'meta_key' => '_order_tax',
            'meta_value' => $_SESSION['cartDetails']['taxAmount']
        );
        $wpdb->insert($wpdb->postmeta, $post_data);
    }
}
unset($_SESSION['cartTotalAmount']);
unset($_SESSION['cartDetails']);
*/
?>

<ul class="order_details">
    <li class="order">
        <?php esc_html_e('Order number:', 'woocommerce'); ?>
        <strong><?php echo esc_html($order->get_order_number()); ?></strong>
    </li>
    <li class="date">
        <?php esc_html_e('Date:', 'woocommerce'); ?>
        <strong><?php echo esc_html(wc_format_datetime($order->get_date_created())); ?></strong>
    </li>
    <li class="total">
        <?php esc_html_e('Total:', 'woocommerce'); ?>
            <strong><?php echo wp_kses_post( $order->get_formatted_order_total() ); ?></strong>
    </li>
    <?php if ($order->get_payment_method_title()) : ?>
        <li class="method">
            <?php esc_html_e('Payment method:', 'woocommerce'); ?>
            <strong><?php echo wp_kses_post($order->get_payment_method_title()); ?></strong>
        </li>
    <?php endif; ?>
</ul>

<?php do_action('woocommerce_receipt_' . $order->get_payment_method(), $order->get_id()); ?>

<div class="clear"></div>
