<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
defined('ABSPATH') || exit;
global $wpdb;

$notes = $order->get_customer_order_notes();

//get delivery details
//get delivery info
$get_order = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}assign_order_deliveryboy WHERE "
        . "order_id = " . $order_id);
$delivery_boy = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}delivery_boy WHERE "
        . "delivery_boy_id = " . $get_order->deliveryboy_id);

?>
<p>
    <?php
    printf(
            /* translators: 1: order number 2: order date 3: order status */
            esc_html__('Order #%1$s was placed on %2$s and is currently %3$s.', 'woocommerce'), '<mark class="order-number">' . $order->get_order_number() . '</mark>', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            '<mark class="order-date">' . wc_format_datetime($order->get_date_created()) . '</mark>', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            '<mark class="order-status">' . wc_get_order_status_name($order->get_status()) . '</mark>' // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    );
    ?>
</p>

<?php if ($get_order) : ?>
    <h2><?php esc_html_e('Delivery updates', 'woocommerce'); ?></h2>
    <?php if ($get_order->delivery_status == 0) { ?>
        <ol class="woocommerce-OrderUpdates commentlist notes">
            <li>
                <span><?php esc_html_e('Delivery Date:', 'dokan-lite'); ?></span>
                <?php echo esc_html(date('d-m-Y', strtotime($get_order->reassigned_date))); ?>
            </li>
            <li>
                <span><?php esc_html_e('Delivery Boy:', 'dokan-lite'); ?></span>
                <?php echo esc_html($delivery_boy->first_name . " " . $delivery_boy->last_name); ?>
            </li>
            <li>
                <span><?php esc_html_e('Verification Code:', 'dokan-lite'); ?></span>
                <?php echo esc_html($get_order->verification_code); ?>
            </li>
        </ol> 
    <?php } elseif ($get_order->delivery_status == 1) { ?>
        <ol class="woocommerce-OrderUpdates commentlist notes">
            <li>
                Your order has been delivered.
            </li>
        </ol>
    <?php } elseif ($get_order->delivery_status == 2) { ?>
        <ol class="woocommerce-OrderUpdates commentlist notes">
            <li>
                Your order has been cancelled.
            </li>
        </ol>
    <?php
    }
endif;
?>

<?php if ($notes) : ?>
    <h2><?php esc_html_e('Order updates', 'woocommerce'); ?></h2>
    <ol class="woocommerce-OrderUpdates commentlist notes">
    <?php foreach ($notes as $note) : ?>
            <li class="woocommerce-OrderUpdate comment note">
                <div class="woocommerce-OrderUpdate-inner comment_container">
                    <div class="woocommerce-OrderUpdate-text comment-text">
                        <p class="woocommerce-OrderUpdate-meta meta"><?php echo date_i18n(esc_html__('l jS \o\f F Y, h:ia', 'woocommerce'), strtotime($note->comment_date)); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped  ?></p>
                        <div class="woocommerce-OrderUpdate-description description">
        <?php echo wpautop(wptexturize($note->comment_content)); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped  ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </li>
    <?php endforeach; ?>
    </ol>
    <?php endif; ?>

<?php do_action('woocommerce_view_order', $order_id); ?>
