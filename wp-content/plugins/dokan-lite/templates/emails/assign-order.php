<?php
/**
 * order Assign Email.
 *
 * An email sent to the vendor when a vendor is contacted via customer.
 *
 * @class       
 * @version     2.6.8
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p>
    <?php esc_html_e( 'Hello '. $data['customername'].',', 'dokan-lite' ); ?>
    <br>
    <?php esc_html_e( 'Your order has been assigned to our delivery boy <strong>'.$data['delivery_boy'] . '</strong> and it will be delivered soon.', 'dokan-lite' ); ?>
</p>
<p>
    <?php esc_html_e( 'At the time of delivery kindly provide the verification code <strong>'. $data['verification_code']. '</strong> to delivery boy for confirmation of your order.', 'dokan-lite' ); ?>
</p>
<hr>

<?php
do_action( 'woocommerce_email_footer', $email );
